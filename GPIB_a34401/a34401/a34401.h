//
//  a34401.h
//  a34401
//
//  Created by MAY on 8/17/12.
//  Copyright (c) 2012 MAY. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface a34401 : NSObject{
    IBOutlet NSTextField *result;
    NSMutableString *result_str;
    int Device;                   /* Device unit descriptor               */
    int BoardIndex;               /* Interface Index (GPIB0=0,GPIB1=1,etc.)*/
    int   PrimaryAddress;      /* Primary address of the device           */
    int   SecondaryAddress;    /* Secondary address of the device         */
}
-(IBAction) Vol:(id)sender;
-(IBAction) Cur:(id)sender;
-(IBAction) Res:(id)sender;
-(IBAction) clr:(id)sender;
-(void) GpibError:(char *)msg;
@end
