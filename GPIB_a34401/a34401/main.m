//
//  main.m
//  a34401
//
//  Created by MAY on 8/17/12.
//  Copyright (c) 2012 MAY. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc, (const char **)argv);
}
