//
//  AppDelegate.h
//  a34401
//
//  Created by MAY on 8/17/12.
//  Copyright (c) 2012 MAY. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>
{
    IBOutlet NSWindow *window;
}

@property (assign) IBOutlet NSWindow *window;

@end
