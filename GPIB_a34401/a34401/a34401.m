//
//  a34401.m
//  a34401
//
//  Created by MAY on 8/17/12.
//  Copyright (c) 2012 MAY. All rights reserved.
//

#import "a34401.h"
#include <NI488/ni488.h>
#define A34401_ADDR 22
#define A34401_CUR_CMD  "MEASure:CURRent:DC?"
#define A34401_VOL_CMD  "MEASure:VOLTage:DC?"
#define A34401_RES_CMD  "MEASure:RESistance?"

@implementation a34401
-(void) awakeFromNib
{
    Device=0;
    BoardIndex=0;
    PrimaryAddress = A34401_ADDR;      /* Primary address of the device           */
    SecondaryAddress = 0;    /* Secondary address of the device         */
    result_str=[[NSMutableString alloc]init];
    [result_str setString:@""];
}

-(IBAction) Vol:(id)sender
{
    //[result setStringValue:@""];
    char  Buffer[101];             /* Read buffer                             */
    [result setStringValue:@""];
    /*****************************************************************************
     * Initialization - Done only once at the beginning of your application.
     *****************************************************************************/
    
    Device = ibdev(                /* Create a unit descriptor handle         */
                   BoardIndex,              /* Board Index (GPIB0 = 0, GPIB1 = 1, ...) */
                   PrimaryAddress,          /* Device primary address                  */
                   SecondaryAddress,        /* Device secondary address                */
                   T10s,                    /* Timeout setting (T10s = 10 seconds)     */
                   1,                       /* Assert EOI line at end of write         */
                   0);                      /* EOS termination mode                    */
    if (ibsta & ERR) {             /* Check for GPIB Error                    */
        [self GpibError:"ibdev Error"];
    }
    
    ibclr(Device);                 /* Clear the device                        */
    if (ibsta & ERR) {
        [self GpibError:"ibclr Error"];
    }
    
    /*****************************************************************************
     * Main Application Body - Write the majority of your GPIB code here.
     *****************************************************************************/
    
    ibwrt(Device, A34401_VOL_CMD, 19);     /* Send the identification query command   */
    if (ibsta & ERR) {
        [self GpibError:"ibwrt Error"];
    }
    
    ibrd(Device, Buffer, 100);     /* Read up to 100 bytes from the device    */
    if (ibsta & ERR) {
        [self GpibError:"ibrd Error"];
    }
    else
        Buffer[ibcntl] = '\0';         /* Null terminate the ASCII string         */
    
    /* Print the device identification         */
    [result setStringValue:[NSString stringWithFormat:@"%s\n", Buffer]];
	
	
    /*****************************************************************************
     * Uninitialization - Done only once at the end of your application.
     *****************************************************************************/
    
    ibonl(Device, 0);              /* Take the device offline                 */
    if (ibsta & ERR) {
        [self GpibError:"ibonl Error"];
    }
    
    ibonl(BoardIndex, 0);          /* Take the interface offline              */
    if (ibsta & ERR) {
        [self GpibError:"ibonl Error"];
    }
}
-(IBAction) Cur:(id)sender
{
    //[result setStringValue:@""];
    char  Buffer[101];             /* Read buffer                             */
    [result setStringValue:@""];
    /*****************************************************************************
     * Initialization - Done only once at the beginning of your application.
     *****************************************************************************/
    
    Device = ibdev(                /* Create a unit descriptor handle         */
                   BoardIndex,              /* Board Index (GPIB0 = 0, GPIB1 = 1, ...) */
                   PrimaryAddress,          /* Device primary address                  */
                   SecondaryAddress,        /* Device secondary address                */
                   T10s,                    /* Timeout setting (T10s = 10 seconds)     */
                   1,                       /* Assert EOI line at end of write         */
                   0);                      /* EOS termination mode                    */
    if (ibsta & ERR) {             /* Check for GPIB Error                    */
        [self GpibError:"ibdev Error"];
    }
    
    ibclr(Device);                 /* Clear the device                        */
    if (ibsta & ERR) {
        [self GpibError:"ibclr Error"];
    }
    
    /*****************************************************************************
     * Main Application Body - Write the majority of your GPIB code here.
     *****************************************************************************/
    
    ibwrt(Device, A34401_CUR_CMD, 19);     /* Send the identification query command   */
    if (ibsta & ERR) {
        [self GpibError:"ibwrt Error"];
    }
    
    ibrd(Device, Buffer, 100);     /* Read up to 100 bytes from the device    */
    if (ibsta & ERR) {
        [self GpibError:"ibrd Error"];
    }
    else
        Buffer[ibcntl] = '\0';         /* Null terminate the ASCII string         */
    
    /* Print the device identification         */
    [result setStringValue:[NSString stringWithFormat:@"%s\n", Buffer]];
	
	
    /*****************************************************************************
     * Uninitialization - Done only once at the end of your application.
     *****************************************************************************/
    
    ibonl(Device, 0);              /* Take the device offline                 */
    if (ibsta & ERR) {
        [self GpibError:"ibonl Error"];
    }
    
    ibonl(BoardIndex, 0);          /* Take the interface offline              */
    if (ibsta & ERR) {
        [self GpibError:"ibonl Error"];
    }
}
-(IBAction) Res:(id)sender
{
    //[result setStringValue:@""];
    char  Buffer[101];             /* Read buffer                             */
    [result setStringValue:@""];
    /*****************************************************************************
     * Initialization - Done only once at the beginning of your application.
     *****************************************************************************/
    
    Device = ibdev(                /* Create a unit descriptor handle         */
                   BoardIndex,              /* Board Index (GPIB0 = 0, GPIB1 = 1, ...) */
                   PrimaryAddress,          /* Device primary address                  */
                   SecondaryAddress,        /* Device secondary address                */
                   T10s,                    /* Timeout setting (T10s = 10 seconds)     */
                   1,                       /* Assert EOI line at end of write         */
                   0);                      /* EOS termination mode                    */
    if (ibsta & ERR) {             /* Check for GPIB Error                    */
        [self GpibError:"ibdev Error"];
    }
    
    ibclr(Device);                 /* Clear the device                        */
    if (ibsta & ERR) {
        [self GpibError:"ibclr Error"];
    }
    
    /*****************************************************************************
     * Main Application Body - Write the majority of your GPIB code here.
     *****************************************************************************/
    
    ibwrt(Device, A34401_RES_CMD, 19);     /* Send the identification query command   */
    if (ibsta & ERR) {
        [self GpibError:"ibwrt Error"];
    }
    
    ibrd(Device, Buffer, 100);     /* Read up to 100 bytes from the device    */
    if (ibsta & ERR) {
        [self GpibError:"ibrd Error"];
    }
    else
        Buffer[ibcntl] = '\0';         /* Null terminate the ASCII string         */
    
    /* Print the device identification         */
    [result setStringValue:[NSString stringWithFormat:@"%s\n", Buffer]];
	
	
    /*****************************************************************************
     * Uninitialization - Done only once at the end of your application.
     *****************************************************************************/
    
    ibonl(Device, 0);              /* Take the device offline                 */
    if (ibsta & ERR) {
        [self GpibError:"ibonl Error"];
    }
    
    ibonl(BoardIndex, 0);          /* Take the interface offline              */
    if (ibsta & ERR) {
        [self GpibError:"ibonl Error"];
    }
    
}

-(IBAction) clr:(id)sender
{
    [result setStringValue:@""];
}

-(void) GpibError:(char *)msg
{
    [result_str setString:[NSString stringWithFormat:@"%@%s\nibsta = 0x%x  <\n",result_str, msg, ibsta]];

    if (ibsta & ERR )  [result_str appendString:@"ERR\n"];
    if (ibsta & TIMO)  [result_str appendString:@"TIMO\n"];
    if (ibsta & END )  [result_str appendString:@"END\n"];
    if (ibsta & SRQI)  [result_str appendString:@"SRQI\n"];
    if (ibsta & RQS )  [result_str appendString:@"RQS\n"];
    if (ibsta & CMPL)  [result_str appendString:@"CMPL\n"];
    if (ibsta & LOK )  [result_str appendString:@"LOK\n"];
    if (ibsta & REM )  [result_str appendString:@"REM\n"];
    if (ibsta & CIC )  [result_str appendString:@"CIC\n"];
    if (ibsta & ATN )  [result_str appendString:@"ATN\n"];
    if (ibsta & TACS)  [result_str appendString:@"TACS\n"];
    if (ibsta & LACS)  [result_str appendString:@"LACS\n"];
    if (ibsta & DTAS)  [result_str appendString:@"DTAS\n"];
    if (ibsta & DCAS)  [result_str appendString:@"DCAS\n"];
    
    [result_str appendString:[NSString stringWithFormat:@"iberr = %d", iberr]];
    if (iberr == EDVR) [result_str appendString:@" EDVR <System Error>\n"];
    if (iberr == ECIC) [result_str appendString:@" ECIC <Not Controller-In-Charge>\n"];
    if (iberr == ENOL) [result_str appendString:@" ENOL <No Listener>\n"];
    if (iberr == EADR) [result_str appendString:@" EADR <Address error>\n"];
    if (iberr == EARG) [result_str appendString:@" EARG <Invalid argument>\n"];
    if (iberr == ESAC) [result_str appendString:@" ESAC <Not System Controller>\n"];
    if (iberr == EABO) [result_str appendString:@" EABO <Operation aborted>\n"];
    if (iberr == ENEB) [result_str appendString:@" ENEB <No GPIB board>\n"];
    if (iberr == EOIP) [result_str appendString:@" EOIP <Async I/O in progress>\n"];
    if (iberr == ECAP) [result_str appendString:@" ECAP <No capability>\n"];
    if (iberr == EFSO) [result_str appendString:@" EFSO <File system error>\n"];
    if (iberr == EBUS) [result_str appendString:@" EBUS <GPIB bus error>\n"];
    if (iberr == ESTB) [result_str appendString:@" ESTB <Status byte lost>\n"];
    if (iberr == ESRQ) [result_str appendString:@" ESRQ <SRQ stuck on>\n"];
    if (iberr == ETAB) [result_str appendString:@" ETAB <Table Overflow>\n"];
    
    [result_str appendString:[NSString stringWithFormat:@"ibcntl = %ld\n", ibcntl]];
    [result setStringValue:result_str];
    /* Call ibonl to take the device and interface offline */
    ibonl (Device,0);
    ibonl (BoardIndex,0);
}
@end
