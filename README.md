#GPIB_NI488_MAC_Cmdline

NI-488.2 for Mac OS X
Copyright National Instruments Corporation.
All Rights Reserved.

The C sample program illustrates how to use the NI-488.2 function
calls to communicate with your instrument.

Open up a Terminal window and navigate to the Sample directory 
located at: Applications>>National Instruments>>NI-488.2>>Sample

This sample application was written to query a device (PAD 2) for its ID.